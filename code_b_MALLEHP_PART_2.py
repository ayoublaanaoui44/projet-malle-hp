# -*- coding: utf-8 -*-


fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

poids_maximal = 4



def tri_insertion(fournitures, criteres):
    for i in range (1, len(fournitures)):
        while fournitures[i][criteres] > fournitures[i - 1][criteres] and i > 0:
            fournitures[i], fournitures[i - 1] = fournitures[i - 1], fournitures[i]
            i = i - 1
    return fournitures

fournitures_triées = tri_insertion(fournitures_scolaires, 'Poids')



def remplissage_malle(fournitures, poids_max):
    malle = {}
    
    for element in fournitures:
        if element['Poids'] <= poids_max:
            malle[element['Nom']] = element['Poids']
            poids_max -= element['Poids']
                        
    return malle

malle_finale = remplissage_malle(fournitures_triées, poids_maximal)


def calcul_poids(malle_remplie):
    poids_final_local = sum(malle_remplie.values())
    return poids_final_local


poids_final = calcul_poids(malle_finale)



def affichage(malle_finale, poids_final):
    print(f'Harry à mit ces objets dans sa malle : ')
    for objet in malle_finale.keys():
        print(objet, end=', ')
    print(f'\nLe poids final de sa valise est de {poids_final} kilos.')

affichage(malle_finale, poids_final)
    