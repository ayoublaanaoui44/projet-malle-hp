from tkinter import*



window = Tk()

window.title("Harry se fait la mala")
window.geometry("980x530")
window.minsize(980, 530)
window.config(background='#629460')

frame = Frame(window, bg='#2B4162', bd=3, relief=SUNKEN)
frame.config(width=1600, height=900)

label_title = Label(frame, text="Bienvenue dans l'IHM :", font=('Elephant', 40, UNDERLINE), bg='#2B4162', fg='#F5F0F6')
label_title.pack()


left_frame = Frame(frame, bg='#2B4162', width=320)
middle_frame = Frame(frame, bg='#2B4162', width=320)
right_frame = Frame(frame, bg='#2B4162', width=320)

left_frame.pack(side=LEFT)
middle_frame.pack(side=BOTTOM)
right_frame.pack(side=RIGHT)


btn1a = Button(left_frame, bg='#385F71', fg='#F5F0F6', text="Remplir la malle…\n n’importe comment ! ", font=('Arial Noir', 20), command=lambda: labela.pack())
btn1a.pack(pady=1)
btn2a = Button(left_frame, bg='#385F71', fg='#F5F0F6', text='Masquer', font=('Gotham Black', 20), command=lambda: labela.pack_forget())
btn2a.pack()
labela = Label(left_frame, bg='#2B4162', fg='#F5F0F6', font='Gotham', text ="Harry à mit ces objets \n dans sa malle : \n - Manuel scolaire \n - Baguette magique \n - Chaudron \n - Robe de travail \nLe poids final de sa \n valise est de 3.635 kilos. \n \n \n \n")
labela.pack()

btn1b = Button(frame, bg='#385F71', fg='#F5F0F6', text="Remplir la malle la \nplus lourde possible", font=('Gotham Black', 20), command=lambda: labelb.pack())
btn1b.pack(pady=1)
btn2b = Button(frame, bg='#385F71', fg='#F5F0F6', text='Masquer', font=('Gotham Black', 20), command=lambda: labelb.pack_forget())
btn2b.pack()
labelb = Label(frame, bg='#2B4162', fg='#F5F0F6', font='Gotham', text = "Harry à mit ces objets \n dans sa malle : \n - Chaudron \n - Balance de cuivre \n - Baguette magique \n Le poids final de sa \n valise est de 3.885 kilos. \n \n \n \n \n")
labelb.pack()

btn1c = Button(right_frame, bg='#385F71', fg='#F5F0F6', text="Remplir la malle avec\n le maximum de mana", font=('Gotham Black', 20), command=lambda: labelc.pack())
btn1c.pack(pady=1)
btn2c = Button(right_frame, bg='#385F71', fg='#F5F0F6', text='Masquer', font=('Gotham Black', 20), command=lambda: labelc.pack_forget())
btn2c.pack()
labelc = Label(right_frame, bg='#2B4162', fg='#F5F0F6', font='Gotham', text = "Harry à mit ces objets \n dans sa malle : \n - Baguette magique \n - Gants \n - Cape \n - Manuel scolaire \n - Chapeau pointu \n - Robe de travail \nLe poids final de sa \n valise est de 3.535 kilos.\nLe mana de tous les objets \n dans la valise est de : 186")
labelc.pack()




frame.pack(expand=YES, side=TOP)



window.mainloop()

